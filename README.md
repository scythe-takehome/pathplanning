# High Level Goal: 
Create a C++ program that can plan a path over an image with known free space and known obstacles from a given start pixel to a given end pixel without colliding with any obstacles. You do not need to factor in kinematics or robot body constraints. 

## Secondary Goal:
Give us a small description about what you would have done to factor robot kinematics into this planner. You do not actually need to write the code to do this. We are just looking for a description about what you would have done. 

### Details: 
The program should be written in C++ and use CMake to build
The program should take three **inputs**:
* Start pixel in (x,y) 
* End pixel in (x,y)
* The image you are operating on 

The **output** should be the image you are operating on with the planned path drawn on it.  
If either a start point or an end point lies inside one of the obstacles the program should say that a plan is not possible.

We have supplied three grayscale images for you to test with. They are in this repository as `path_planning_challenge_image_*.png`. Feel free to make your own as well.  
In our supplied images the “free space” pixel values are 0 and the “obstacle” pixels values are 255

You can use whatever path planning algorithm you would like. Yes, faster computation or a more optimal path is better, but the most important thing is that the path goes from start to end without a collision.  
You can assume that the width of the robot is 1 pixel and that it is holonomic. So no need to factor in any robot dimensions or kinematics.  
This program will require you to manipulate an image. You are free to do that however you choose. We recommend using OpenCV but if you use another library that is okay too as long as you provide information for how to install said library.  
You don’t need to spend a TON of time on this challenge. If you are spending 4+ hours on this feel free to send us the code where it is at along with what you are blocked on and what you would have done if you had more time. 


### What to send back? 
* Source code of your program
* Add a text file containing instructions for how to build and run your program, a list of dependencies needed to build this program (but try to keep the dependencies as minimal as possible), and the description of what you would have done to factor in robot body constraints
* Any test images you created yourself or anything else you think we will need

### How to send it back?
* Send us a .tar or .zip file containing all the above in the same thread where you scheduled all our interviews.

#### If anything is unclear about what you need to do please reach out and we will clarify.
